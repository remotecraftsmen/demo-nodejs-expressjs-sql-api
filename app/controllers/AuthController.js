const {User} = require('../models');
const Auth = require('../services/Auth');
const HttpStatus = require('http-status-codes');

class AuthController {

    /**
     *  @api {post} /auth/login Attempt to log
     *  @apiName PostAuthLogin
     *  @apiGroup Auth
     *  @apiVersion 1.0.0
     *
     *  @apiParam {String} email Users email
     *  @apiParam {String} password Users password
     *
     *  @apiParamExample {json} Request-Example:
     *     {
     *       "auth_token": ""
     *       "email": "test@test.com"
     *       "password": "123456"
     *     }
     *
     *  @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *    {
     *        "auth": true,
     *        "token": TOKEN GENERATED BY JWT,
     *        "user": {
     *            "id": 6,
     *            "username": "aaaa",
     *            "first_name": "aaaaa",
     *            "last_name": "aaaaa",
     *            "email": "test@test.com"
     *        }
     *    }
     *
     *  @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *       "auth": false,
     *       "token": null,
     *        "user": null
     *     }
     */
    static async login(request, response) {
        const {email, password} = request.body;

        const user = await User.findOne({
            where: {email}, attributes: ['id', 'username', 'first_name', 'last_name', 'email', 'password']
        });

        if (!user) {
            return response.status(HttpStatus.UNAUTHORIZED).json({auth: false, token: null});
        }

        if (Auth.checkCredentials(password, user.getDataValue('password'))) {
            const token = Auth.signIn(user);
            let plainUser = user.get({plain: true});
            delete plainUser.password;

            return response.json({auth: true, token, user: plainUser});
        }

        return response.status(HttpStatus.UNAUTHORIZED).json({auth: false, token: null, user: null});
    }
}

module.exports = AuthController;
