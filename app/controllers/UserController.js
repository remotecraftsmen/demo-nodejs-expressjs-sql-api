const Auth = require('../services/Auth');
const {User} = require('../models');
const HttpStatus = require('http-status-codes');

class UserController {

    /**
     *  @api {get} /users Read all Users
     *  @apiName GetUserIndex
     *  @apiGroup Users
     *  @apiVersion 1.0.0
     *
     *  @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *    {
     *        "users": [
     *            {
     *                "id": 1,
     *                "username": "uname",
     *                "first_name": "fname",
     *                "last_name": "lname",
     *                "email": "me@me1543244202844.com",
     *                "created_at": "2018-11-26T14:56:43.003Z",
     *                "updated_at": "2018-11-26T14:56:43.003Z"
     *            }
     *        ]
     *    }
     *
     *   @apiSuccess {Number}      id        
     *   @apiSuccess {String}      username  
     *   @apiSuccess {String}      first_name
     *   @apiSuccess {String}      last_name 
     *   @apiSuccess {String}      email     
     *   @apiSuccess {Timestamp}   updated_at
     *   @apiSuccess {Timestamp}   created_at
     *
     *   @apiError (400) BadRequest
     */
    static getCollection(request, response, next) {
        User.findAll()
            .then(users => {
                response.json({users});
            })
            .catch(next);
    }

    /**
     *  @api {post} /users Create User
     *  @apiName PostUserStore
     *  @apiGroup Users
     *  @apiVersion 1.0.0
     *
     *  @apiParam {String} username
     *  @apiParam {String} first_name
     *  @apiParam {String} last_name 
     *  @apiParam {String} email
     *  @apiParam {String} password
     *
     *  @apiParamExample {json} Request-Example:
     *   {
     *   	"username": "aaaa",
     *   	"first_name": "aaaaa",
     *   	"last_name": "aaaaa",
     *   	"email": "test123@test.com",
     *   	"password": "123456"
     *   }
     *
     *  @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *    {
     *        "auth": true,
     *        "token": TOKEN GENERATED BY JWT,
     *        "user": {
     *            "id": 6,
     *            "username": "aaaa",
     *            "first_name": "aaaaa",
     *            "last_name": "aaaaa",
     *            "updated_at": "2018-11-27T11:57:02.003Z",
     *            "created_at": "2018-11-27T11:57:02.003Z"
     *        }
     *    }
     *
     *  @apiSuccess {Boolean}    auth            
     *  @apiSuccess {String}     token           
     *  @apiSuccess {Object}     user            
     *
     *  @apiError (400) BadRequest
     *
     */
    static async storeItem(request, response, next) {
        await User.create({...request.body})
            .then(user => {
                const token = Auth.signIn(user);

                let plainUser = user.get({plain: true});
                delete plainUser.password;

                return response.json({auth: true, token, user: plainUser});
            })
            .catch(next);
    }

    /**
     *  @api {get} /users/:id Show User
     *  @apiName GetUserShow
     *  @apiGroup Users
     *  @apiVersion 1.0.0
     *
     *  @apiParam {Number} id             ID of a User
     *
     *  @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       OK
     *
     *   @apiError (404) Not Found    The User with <code>id</code> was not found.
     */
    static getItem(request, response, next) {
        const user_id = request.params.id;

        User.findByPk(user_id)
            .then(user => {
                if (!user) {
                    return response.sendStatus(HttpStatus.NOT_FOUND);
                }

                response.json(user);
            })
            .catch(next);
    }

    /**
     *  @api {put} /users Update User
     *  @apiName PutUserUpdate
     *  @apiGroup Users
     *  @apiVersion 1.0.0
     *
     *  @apiParam {String} username 
     *  @apiParam {String} first_name
     *  @apiParam {String} last_name 
     *  @apiParam {String} email User
     *  @apiParam {String} password
     *
     *  @apiParamExample {json} Request-Example:
     *   {
     *   	"username": "aaaa",
     *   	"first_name": "aaaaa",
     *   	"last_name": "aaaaa",
     *   	"email": "test123@test.com",
     *   	"password": "123456"
     *   }
     *
     *  @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *    {
     *        "auth": true,
     *        "token": TOKEN GENERATED BY JWT,
     *        "user": {
     *            "id": 6,
     *            "username": "aaaa",
     *            "first_name": "aaaaa",
     *            "last_name": "aaaaa",
     *            "updated_at": "2018-11-27T11:57:02.003Z",
     *            "created_at": "2018-11-27T11:57:02.003Z"
     *        }
     *    }
     *
     *  @apiSuccess {Boolean}    auth           
     *  @apiSuccess {String}     token          
     *  @apiSuccess {Object}     user           
     *
     *   @apiError NotFound     The User with <code>id</code> was not found.
     *   @apiError Forbidden    Users can delete only themselfs
     *   @apiError BadRequest
     *
     */
    static putItem(request, response, next) {
        const user_id = request.params.id;
        const fields = request.body;

        User.findByPk(user_id).then(user => {
            if (!user) {
                return response.sendStatus(HttpStatus.NOT_FOUND);
            }

            if (user.id !== request.logged_user_id) {
                return response.sendStatus(HttpStatus.FORBIDDEN);
            }

            user.update(fields)
                .then(() => {
                    response.sendStatus(HttpStatus.OK);
                })
                .catch(next);
        });
    }

    /**
     *  @api {delete} /users/:id Delete User
     *  @apiName DeleteUserDestroy
     *  @apiGroup Users
     *  @apiVersion 1.0.0
     *
     *  @apiParam {Number} id             ID of a ToDo List element
     *
     *  @apiSuccessExample Success-Response:
     *     HTTP/1.1 204 No Content
     *
     *   @apiError NotFound     The User with <code>id</code> was not found.
     *   @apiError Forbidden    Users can delete only themselfs
     *   @apiError BadRequest
     */
    static destroyItem(request, response, next) {
        const user_id = request.params.id;

        User.findByPk(user_id).then(user => {
            if (!user) {
                return response.sendStatus(HttpStatus.NOT_FOUND);
            }

            if (user.id !== request.logged_user_id) {
                return response.sendStatus(HttpStatus.FORBIDDEN);
            }

            user.destroy()
                .then(() => {
                    response.sendStatus(HttpStatus.NO_CONTENT);
                })
                .catch(next);
        });
    }
}

module.exports = UserController;
